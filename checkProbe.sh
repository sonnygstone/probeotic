#!/usr/bin/bash

while true
do
  echo "checking logs for errors now"
  results="$(grep 'Setting state to dead' /logs/probeotic.log | wc -l)"

  if [ $(results) -gt  0 ]
  then
    # remove ready file
    rm /data/ready
  else
    # add ready file
    #touch /data/ready
  fi

  results="$(grep 'Setting state to slow' /logs/probeotic.log | wc -l)"

  if [ $(results) -gt  0 ]
  then
    # remove healthy file
    rm /data/healthy
  else
    # add healthy file
    #touch /data/healthy
  fi

  results="$(grep 'Setting state to normal' /logs/probeotic.log | wc -l)"

  if [ $(results) -gt  0 ]
  then
    # remove healthy file
    touch /data/ready
  else
    # add healthy file
    touch /data/healthy
  fi
  sleep 10
done
