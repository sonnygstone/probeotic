package com.capstone.probeotic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProbeoticApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProbeoticApplication.class, args);
    }

}
