package com.capstone.probeotic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequestMapping("/probe")
public class ProbeController {

    Logger logger = LoggerFactory.getLogger(ProbeController.class);

    @PostMapping()
    public ResponseEntity<Void> setProbeState(@RequestParam String state) {
        logger.info("Setting state to {}", state);

        return ResponseEntity
                .status(HttpStatus.FOUND)
                .location(URI.create("/?state=" + state))
                .build();
    }
}
